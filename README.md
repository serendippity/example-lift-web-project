Given the amount of trouble gone through to figure out how to create a Lift project and make it run (with triggered
reload), this repo was created so that anyone can clone it and have a web project ready to go.

The project uses a custom *HTML5 Boilerplate*, which can be found here: [https://html5boilerplate.com/]
(https://html5boilerplate.com/)

To run with automatic reloads when files change:
    # sbt

    > ~jetty:start


__May the coding gods be merciful__
