val liftVersion = "3.0-RC3"

lazy val root = (project in file(".")).
  settings(
    organization := "serendipity",
    name := "story-telling",
    version := "1.0",
    scalaVersion := "2.11.8",
    libraryDependencies ++= Seq(
      "net.liftweb" %% "lift-webkit" % liftVersion % "compile",
      "org.eclipse.jetty" % "jetty-webapp" % "9.3.13.v20161014"  %
        "container,test",
      "org.eclipse.jetty.orbit" % "javax.servlet" % "3.0.0.v201112011016" %
        "container,compile" artifacts Artifact("javax.servlet", "jar", "jar")
    )
  )
  .enablePlugins(JettyPlugin)

//
//watchSources ~= { files =>
//  println(files.mkString("\n")+"\n\n\n")
//  files//here you can add files or filter out
//}